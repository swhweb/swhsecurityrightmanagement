﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands.Handlers
{
    internal class HelpCommandHandler : CommandHandlerBase
    {
        internal override string Command => "?";

        public override CommandResult Handle(string command)
        {
            Console.WriteLine("Elérhető parancsok:");
            Console.WriteLine();
            Console.WriteLine("?\t\tSegítség kérése");
            Console.WriteLine();
            Console.WriteLine("exit\t\tKilépés");
            Console.WriteLine();
            Console.WriteLine("list\t\tGenerált rekordok listázása konzolra");
            Console.WriteLine("\t-new\tGenerált, adatbázisban nem szereplő rekordok listázása");
            Console.WriteLine("\t-sds, -mvc\tCsak a megadott projekt alapján generálódó rekordok listázása");
            Console.WriteLine();
            Console.WriteLine("write\t\tGenerált új rekordok írása adatbázisba");
            Console.WriteLine("\t-sds, -mvc\tCsak a megadott projekt alapján generálódó rekordok írása");
            Console.WriteLine();

            return CommandResult.Success();
        }
    }
}
