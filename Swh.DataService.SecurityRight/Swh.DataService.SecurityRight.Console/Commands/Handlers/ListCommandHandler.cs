﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.DataAccess;
using Swh.DataService.SecurityRight.Generator;

namespace Swh.DataService.SecurityRight.Commands.Handlers
{
    internal class ListCommandHandler : CommandHandlerBase
    {
        private readonly ISecurityRightConfiguration configuration;
        private readonly ISecurityRightGenerator securityRightGenerator;
        private readonly ISecurityRightService securityRightService;

        internal override string Command => "list";

        public ListCommandHandler(ISecurityRightConfiguration configuration, ISecurityRightGenerator securityRightGenerator, ISecurityRightService securityRightService)
        {
            this.configuration = configuration;
            this.securityRightGenerator = securityRightGenerator;
            this.securityRightService = securityRightService;
        }

        public override CommandResult Handle(string command)
        {
            var switches = GetSwitches(command);

            var rights = securityRightGenerator.GetSecurityRights(GetNames(GetSwitches(command)));

            if (switches.Contains("new"))
            {
                rights = rights.Where(right => !securityRightService.Exists(right));
            }

            foreach (var right in rights)
            {
                Console.WriteLine(right.ToString());
            }

            return CommandResult.Success($"{rights.Count()} darab.");
        }

        private IEnumerable<string> GetNames(IEnumerable<string> switches)
        {
            var assemblies = configuration.GetAssemblyNames(switches);

            if (assemblies.Count() == 0)
            {
                assemblies = configuration.Assemblies.Select(a => a.Name);
            }

            return assemblies;
        }
    }
}
