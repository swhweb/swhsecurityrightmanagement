﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SimpleInjector;

using Swh.DataService.SecurityRight.Application;
using Swh.DataService.SecurityRight.Commands;
using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.DataAccess;
using Swh.DataService.SecurityRight.Generator;
using Swh.DataService.SecurityRight.Logging;

namespace Swh.DataService.SecurityRight.DI
{
    internal static class SimpleInjectorConfig
    {
        internal static Container CreateContainer(ISecurityRightConfiguration configuration, string[] arguments)
        {
            var container = new Container();

            RegisterServices(container, configuration, arguments);

            container.Verify();
            return container;
        }

        private static void RegisterServices(Container container, ISecurityRightConfiguration configuration, string[] arguments)
        {
            container.Register<ICommandHandler, CommandDispatcher>();
            container.RegisterDecorator<ICommandHandler, CommandLogger>();
            container.Collection.Register<CommandHandlerBase>(new[] { typeof(SimpleInjectorConfig).Assembly });

            container.Register(() => SecurityRightConfiguration.Current, Lifestyle.Singleton);

            container.Collection.Register<SecurityRightGeneratorHelperBase>(new[] { typeof(SimpleInjectorConfig).Assembly });
            container.Register<ISecurityRightGenerator, SecurityRightGenerator>();
            //container.RegisterDecorator<ISecurityRightGenerator, SecurityRightGeneratorCache>();

            container.Register<ISecurityRightService>(() => new SecurityRightService(configuration.DataService, container.GetInstance<ILogger>()));

            container.Register<ILogger, Logger>();

            RegisterEnvironmentDependencies(container, configuration.Environment, arguments);
        }

        private static void RegisterEnvironmentDependencies(Container container, ISecurityRightEnvironmentConfiguration environment, string[] arguments)
        {
            switch (environment.Mode)
            {
                case EnvironmentMode.Default:

                    container.Register<IExecution, UserInputExecution>();

                    container.RegisterDecorator<ICommandHandler, ErrorCatherCommandHandler>();

                    break;

                case EnvironmentMode.TeamCity:

                    container.Register<IExecution, ArgumentExecution>();

                    container.Register<GetArguments>(() => () => arguments);

                    break;
            }
        }
    }
}
