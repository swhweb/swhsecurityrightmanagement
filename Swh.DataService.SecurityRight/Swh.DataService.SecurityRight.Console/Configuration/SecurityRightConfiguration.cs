﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    internal class SecurityRightConfiguration : ISecurityRightConfiguration
    {
        private static Lazy<ISecurityRightConfiguration> current;

        internal static ISecurityRightConfiguration Current => current.Value;

        static SecurityRightConfiguration()
        {
            current = new Lazy<ISecurityRightConfiguration>(Read);
        }

        private readonly Lazy<ISecurityRightDataServiceConfiguration> dataService;
        private readonly Lazy<ISecurtiyRightAssemblyConfiguration[]> assemblies;

        #region implementing ISecurityRightConfiguration

        public ISecurityRightDataServiceConfiguration DataService => dataService.Value;

        public ISecurtiyRightAssemblyConfiguration[] Assemblies => assemblies.Value;

        public ISecurityRightEnvironmentConfiguration Environment { get; }

        #endregion

        private SecurityRightConfiguration(SecurityRightConfigurationSection section)
        {
            dataService = new Lazy<ISecurityRightDataServiceConfiguration>(() => 
                new SecurityRightDataServiceConfiguration(section.DataService));

            assemblies = new Lazy<ISecurtiyRightAssemblyConfiguration[]>(() => 
                section.Assemblies.Elements.Select(item => new SecurityRightAssemblyConfiguration(item)).ToArray());

            Environment = new SecurityRightEnvironmentConfiguration(section.Environment.Mode);
        }

        private static ISecurityRightConfiguration Read()
        {
            return new SecurityRightConfiguration((SecurityRightConfigurationSection)ConfigurationManager.GetSection("swh.securityRight"));
        }
    }
}
