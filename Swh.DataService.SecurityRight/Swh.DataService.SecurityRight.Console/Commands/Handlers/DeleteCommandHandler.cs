﻿using Swh.DataService.SecurityRight.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands.Handlers
{
    internal class DeleteCommandHandler : CommandHandlerBase
    {
        private readonly ISecurityRightService service;

        internal override string Command => "delete";

        public DeleteCommandHandler(ISecurityRightService service)
        {
            this.service = service;
        }

        public override CommandResult Handle(string command)
        {
            var lineCount = service.DeleteAll();

            return CommandResult.Success($"{lineCount} rekord törlésre került.");
        }
    }
}
