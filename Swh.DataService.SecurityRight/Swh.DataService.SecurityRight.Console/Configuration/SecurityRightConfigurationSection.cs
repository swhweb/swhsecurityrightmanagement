﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    public class SecurityRightConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("dataservice")]
        public DataServiceConfigurationElement DataService
        {
            get => (DataServiceConfigurationElement)this["dataservice"];
            set => this["dataservice"] = value;
        }

        [ConfigurationProperty("assemblies")]
        public AssemblyCollectionConfiguration Assemblies
        {
            get => (AssemblyCollectionConfiguration)this["assemblies"];
            set => this["assemblies"] = value;
        }

        [ConfigurationProperty("environment")]
        public EnvironmentConfigurationElement Environment
        {
            get => (EnvironmentConfigurationElement)this["environment"];
            set => this["environment"] = value;
        }
    }
}
