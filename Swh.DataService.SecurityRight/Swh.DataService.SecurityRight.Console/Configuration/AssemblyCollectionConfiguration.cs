﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    [ConfigurationCollection(typeof(AssemblyConfigurationElement))]
    public class AssemblyCollectionConfiguration : ConfigurationElementCollection
    {
        protected override string ElementName => "assembly";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;

        protected override ConfigurationElement CreateNewElement()
        {
            return new AssemblyConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((AssemblyConfigurationElement)element).Name;
        }

        internal IEnumerable<AssemblyConfigurationElement> Elements
        {
            get
            {
                foreach (var item in this)
                {
                    yield return (AssemblyConfigurationElement)item;
                }
            }
        }
    }
}
