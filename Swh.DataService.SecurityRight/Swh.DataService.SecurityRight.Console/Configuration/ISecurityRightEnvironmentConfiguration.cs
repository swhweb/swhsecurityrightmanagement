﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    internal interface ISecurityRightEnvironmentConfiguration
    {
        EnvironmentMode Mode { get; }
    }
}
