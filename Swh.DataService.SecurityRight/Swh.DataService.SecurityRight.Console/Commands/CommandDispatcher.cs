﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class CommandDispatcher : ICommandHandler
    {
        private readonly IEnumerable<CommandHandlerBase> handlers;

        public CommandDispatcher(IEnumerable<CommandHandlerBase> handlers)
        {
            this.handlers = handlers;
        }

        public CommandResult Handle(string command)
        {
            var handler = (ICommandHandler)handlers.SingleOrDefault(MatchesCommand(command)) ?? new DefaultCommandHandler();

            return handler.Handle(command);
        }

        private static Func<CommandHandlerBase, bool> MatchesCommand(string command)
            => (handler) => handler.Command == command.Split(' ')[0];
    }
}
