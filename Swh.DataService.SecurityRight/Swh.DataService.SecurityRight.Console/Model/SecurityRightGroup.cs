﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Model
{
    internal enum SecurityRightGroup
    {
        View,
        Entity,
        CommandQuery,
        BusinessRule,
    }
}
