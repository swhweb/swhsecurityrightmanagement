﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    public class EnvironmentConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("mode")]
        public EnvironmentMode Mode
        {
            get => (EnvironmentMode)this["mode"];
            set => this["mode"] = value;
        }
    }
}
