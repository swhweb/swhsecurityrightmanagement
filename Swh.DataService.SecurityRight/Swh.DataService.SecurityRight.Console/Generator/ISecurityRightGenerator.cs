﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Generator
{
    internal interface ISecurityRightGenerator
    {
        IEnumerable<Model.SecurityRight> GetSecurityRights(IEnumerable<string> modules);
    }
}
