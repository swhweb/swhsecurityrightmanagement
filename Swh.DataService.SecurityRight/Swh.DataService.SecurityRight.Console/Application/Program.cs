﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.DI;

namespace Swh.DataService.SecurityRight.Application
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Console.WriteLine($"SWH SecurityRight Manager v{Assembly.GetExecutingAssembly().GetName().Version}\n");

            var configuration = SecurityRightConfiguration.Current;

            var container = SimpleInjectorConfig.CreateContainer(configuration, args);

            var execution = container.GetInstance<IExecution>();

            return execution.Execute().WindowsErrorCode;
        }
    }
}
