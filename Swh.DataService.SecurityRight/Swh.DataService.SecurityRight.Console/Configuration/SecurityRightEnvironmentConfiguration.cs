﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    public class SecurityRightEnvironmentConfiguration : ISecurityRightEnvironmentConfiguration
    {
        public EnvironmentMode Mode { get; }

        public SecurityRightEnvironmentConfiguration(EnvironmentMode mode)
        {
            Mode = mode;
        }
    }
}
