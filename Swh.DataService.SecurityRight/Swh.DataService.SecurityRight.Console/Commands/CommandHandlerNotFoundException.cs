﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class CommandHandlerNotFoundException : Exception
    {
        public CommandHandlerNotFoundException(string command)
            :base($"'{command}' nem ertelmezheto parancskent. Segitseghez futtasd a '?' parancsot.")
        {

        }
    }
}
