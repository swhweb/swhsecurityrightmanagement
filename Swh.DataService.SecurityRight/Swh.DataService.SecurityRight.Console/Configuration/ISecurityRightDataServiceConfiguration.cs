﻿namespace Swh.DataService.SecurityRight.Configuration
{
    internal interface ISecurityRightDataServiceConfiguration
    {
        string AppTenantId { get; }
        string ConnectionString { get; }
        string UserId { get; }
    }
}