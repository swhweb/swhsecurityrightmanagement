﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Model;

namespace Swh.DataService.SecurityRight.Logging
{
    internal interface ILogger
    {
        void Log(params string[] lines);
        void LogException(Exception e);
        void LogDatabaseWrite(SecurityRightDTO securityRight);
    }
}
