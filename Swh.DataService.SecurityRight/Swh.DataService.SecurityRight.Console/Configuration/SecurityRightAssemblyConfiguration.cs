﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    class SecurityRightAssemblyConfiguration : ISecurtiyRightAssemblyConfiguration
    {
        private readonly Lazy<string> absolutePath;
        private readonly Lazy<string> name;
        private readonly Lazy<string> @switch;

        #region implementing ISecurityRightAssemblyConfiguration

        public string AbsolutePath => absolutePath.Value;

        public string Name => name.Value;

        public string Switch => @switch.Value;

        #endregion

        public SecurityRightAssemblyConfiguration(AssemblyConfigurationElement element)
        {
            absolutePath = new Lazy<string>(() => element.AbsolutePath);
            name = new Lazy<string>(() => element.Name);
            @switch = new Lazy<string>(() => element.Switch);
        }
    }
}
