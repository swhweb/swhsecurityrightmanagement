﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.DataAccess
{
    internal interface ISecurityRightService
    {
        int Write(IEnumerable<Model.SecurityRight> securityRights);
        bool Exists(Model.SecurityRight securityRight);
        int DeleteAll();
    }
}
