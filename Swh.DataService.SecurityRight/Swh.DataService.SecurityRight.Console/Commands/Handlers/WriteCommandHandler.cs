﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.DataAccess;
using Swh.DataService.SecurityRight.Generator;

namespace Swh.DataService.SecurityRight.Commands.Handlers
{
    internal class WriteCommandHandler : CommandHandlerBase
    {
        private readonly ISecurityRightConfiguration configuration;
        private readonly ISecurityRightGenerator securityRightGenerator;
        private readonly ISecurityRightService securityRightService;

        internal override string Command => "write";

        public WriteCommandHandler(ISecurityRightConfiguration configuration, ISecurityRightGenerator securityRightGenerator, ISecurityRightService securityRightService)
        {
            this.configuration = configuration;
            this.securityRightGenerator = securityRightGenerator;
            this.securityRightService = securityRightService;
        }

        public override CommandResult Handle(string command)
        {
            var switches = GetSwitches(command);

            var rights = securityRightGenerator.GetSecurityRights(GetNames(switches))
                .Where(r => !securityRightService.Exists(r));

            var count = securityRightService.Write(rights);

            return CommandResult.Success($"{count} rekord beszurasra kerult.");
        }

        private IEnumerable<string> GetNames(IEnumerable<string> switches)
        {
            var assemblies = configuration.GetAssemblyNames(switches);

            if (assemblies.Count() == 0)
            {
                assemblies = configuration.Assemblies.Select(a => a.Name);
            }

            return assemblies;
        }
    }
}
