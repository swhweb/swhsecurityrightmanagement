﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands
{
    internal abstract class CommandHandlerBase : ICommandHandler
    {
        public abstract CommandResult Handle(string command);

        internal abstract string Command { get; }

        protected internal static string[] GetSwitches(string command)
        {
            return command.Split(' ')
                .Where(item => item.StartsWith("-"))
                .Select(item => item.TrimStart('-'))
                .ToArray();
        }
    }
}
