﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Commands;

namespace Swh.DataService.SecurityRight.Application
{
    internal class ArgumentExecution : IExecution
    {
        private readonly ICommandHandler commandHandler;
        private readonly GetArguments getArguments;

        public ArgumentExecution(ICommandHandler commandHandler, GetArguments getArguments)
        {
            this.commandHandler = commandHandler;
            this.getArguments = getArguments;
        }

        public ResultCode Execute()
        {
            var arguments = getArguments();

            var command = string.Join(" ", arguments);

            var commandResult = commandHandler.Handle(command);

            return ResultCode.FromCommandResult(commandResult);
        }
    }
}
