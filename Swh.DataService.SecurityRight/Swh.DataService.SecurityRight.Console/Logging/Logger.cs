﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Model;

namespace Swh.DataService.SecurityRight.Logging
{
    internal class Logger : ILogger
    {
        private string TimeStamp => $"[{DateTime.Now.ToString(("yyyy-MM-dd HH:mm:ss"))}]";

        public void LogDatabaseWrite(SecurityRightDTO securityRight)
        {
            Log($"Wrote record: {securityRight.Id} {securityRight.Name}");
        }

        public void LogException(Exception e)
        {
            Log("Exception occured:",
                e.Message,
                e.StackTrace);
        }

        public void Log(params string[] lines)
        {
            var date = DateTime.Now;

            Directory.CreateDirectory("Log");
            File.AppendAllLines($"Log/{date.Year}-{date.Month}-{date.Day}.log.txt", lines.Select(line => $"{TimeStamp} {line}"));
        }
    }
}
