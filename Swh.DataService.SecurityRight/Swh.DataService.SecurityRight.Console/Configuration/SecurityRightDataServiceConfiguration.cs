﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    class SecurityRightDataServiceConfiguration : ISecurityRightDataServiceConfiguration
    {
        private readonly DataServiceConfigurationElement element;

        private readonly Lazy<string> appTenantId;
        private readonly Lazy<string> connectionString;
        private readonly Lazy<string> userId;

        #region implementing ISecurityRightDataServiceConfiguration

        public string AppTenantId => appTenantId.Value;

        public string ConnectionString => connectionString.Value;

        public string UserId => userId.Value;

        #endregion

        public SecurityRightDataServiceConfiguration(DataServiceConfigurationElement element)
        {
            this.element = element;

            connectionString = new Lazy<string>(() => element.ConnectionString);
            appTenantId = new Lazy<string>(() => element.ApptenantId);
            userId = new Lazy<string>(() => element.UserId);
        }
    }
}
