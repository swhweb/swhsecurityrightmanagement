﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Model
{
    internal class SecurityRight
    {
        public string Scope { get; set; }
        public SecurityRightGroup Group { get; set; }
        public SecurityRightType Type { get; set; }

        public override string ToString()
        {
            if (Group == SecurityRightGroup.BusinessRule)
            {
                return Scope;
            }

            return $"{Scope}_{Group}_{Type}";
        }
    }
}
