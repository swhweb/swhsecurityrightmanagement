﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.Logging;
using Swh.DataService.SecurityRight.Model;

namespace Swh.DataService.SecurityRight.DataAccess
{
    internal class SecurityRightService : ISecurityRightService
    {
        private readonly ISecurityRightDataServiceConfiguration configuration;
        private readonly ILogger logger;

        public SecurityRightService(ISecurityRightDataServiceConfiguration configuration, ILogger logger)
        {
            this.configuration = configuration;
            this.logger = logger;
        }

        private const string GetIdenticalQueryString =
            "SELECT [SecurityRightId] " +
                "FROM [SwhWebHouse].[dbo].[SecurityRight] " +
                "WHERE [Name] = @name AND  " +
                    "[KeyValueIDSecurityRightType] = @type AND  " +
                    "([EntityName] = @entityName OR (@entityName IS NULL AND [EntityName] IS NULL)) AND  " +
                    "([ViewName] = @viewName OR (@viewName IS NULL AND [ViewName] IS NULL)) AND  " +
                    "([CqName] = @cqName OR (@cqName IS NULL AND [CqName] IS NULL)) AND  " +
                    "[BusinessRule] = @businessRule;";

        public bool Exists(Model.SecurityRight securityRight)
        {
            using (var connection = CreateConnection())
            {
                var dto = CreateDTO(securityRight);

                var command = new SqlCommand(GetIdenticalQueryString, connection);
                command.Parameters.AddWithValue("@name", dto.Name);
                command.Parameters.AddWithValue("@type", dto.KeyValueIdSecurityRigthType);
                command.Parameters.AddWithValue("@entityName", dto.EntityName ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@viewName", dto.ViewName ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@cqName", dto.CqName ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@businessRule", dto.BusinessRule);

                connection.Open();
                var reader = command.ExecuteReader();

                return reader.Read();
            }
        }

        private const string InsertCommandString =
            "INSERT INTO [SwhWebHouse].[dbo].[SecurityRight] " +
                "( SecurityRightId, SecurityRightIDA, AppTenantId, Name, KeyValueIDSecurityRightType, EntityName, ViewName, CqName, BusinessRule, UserIDR, SecurityRightDR, SecurityRightRVF ) " +
                "VALUES ( @id, @ida, @appTenantId, @name, @type, @entityName, @viewName, @cqName, @businessRule, @user, @dr, @rvf) ;";

        public int Write(IEnumerable<Model.SecurityRight> securityRights)
        {
            int rowCounter = 0;

            using (var connection = CreateConnection())
            {
                connection.Open();

                foreach (var item in securityRights)
                {
                    var dto = CreateDTO(item);

                    var command = new SqlCommand(InsertCommandString, connection);

                    command.Parameters.AddWithValue("@ida", true);
                    command.Parameters.AddWithValue("@apptenantId", string.IsNullOrEmpty(configuration.AppTenantId) ? (object)DBNull.Value : configuration.AppTenantId);
                    command.Parameters.AddWithValue("@user", configuration.UserId);
                    command.Parameters.AddWithValue("@dr", DateTimeOffset.Now);
                    command.Parameters.AddWithValue("@rvf", DateTimeOffset.Now);

                    command.Parameters.AddWithValue("@id", dto.Id);
                    command.Parameters.AddWithValue("@name", dto.Name);
                    command.Parameters.AddWithValue("@type", dto.KeyValueIdSecurityRigthType);
                    command.Parameters.AddWithValue("@entityName", dto.EntityName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@viewName", dto.ViewName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@cqName", dto.CqName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@businessRule", dto.BusinessRule);

                    rowCounter += command.ExecuteNonQuery();

                    logger.LogDatabaseWrite(dto);
                }
            }

            return rowCounter;
        }

        private const string DeleteAllCommandString =
            "DELETE FROM [SwhWebHouse].[dbo].[SecurityRight] " +
                "WHERE ([AppTenantID] = @tenantId OR (@tenantId IS NULL AND [AppTenantID] IS NULL));";

        public int DeleteAll()
        {
            using (var connection = CreateConnection())
            {
                var command = new SqlCommand(DeleteAllCommandString, connection);

                command.Parameters.AddWithValue("@tenantId",  configuration.AppTenantId);

                connection.Open();

                return command.ExecuteNonQuery();
            }
        }

        private SqlConnection CreateConnection() => new SqlConnection(configuration.ConnectionString);

        private const string GetSecurityRightTypeIdQueryString =
            "SELECT TOP 1 [KeyValueId] " +
                "FROM [SwhWebHouse].[dbo].[KeyValue]" +
                "WHERE [Scope] = 'SecurityRightType' AND [Key] = @key;";

        private SecurityRightDTO CreateDTO(Model.SecurityRight securityRight)
        {
            Guid typeId;

            using (var connection = CreateConnection())
            {
                var command = new SqlCommand(GetSecurityRightTypeIdQueryString, connection);
                command.Parameters.AddWithValue("@key", securityRight.Type.ToString());

                connection.Open();
                var reader = command.ExecuteReader();

                reader.Read();

                typeId = Guid.Parse(reader[0].ToString());
            }

            return new SecurityRightDTO
            {
                BusinessRule = securityRight.Group == SecurityRightGroup.BusinessRule,
                CqName = securityRight.Group == SecurityRightGroup.CommandQuery ? securityRight.Scope : null,
                ViewName = securityRight.Group == SecurityRightGroup.View ? securityRight.Scope : null,
                EntityName = securityRight.Group == SecurityRightGroup.Entity ? securityRight.Scope : null,
                Name = securityRight.ToString(),
                KeyValueIdSecurityRigthType = typeId,
            };
        }

    }
}
