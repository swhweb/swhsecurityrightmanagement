﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Generator
{
    internal class SecurityRightGeneratorCache : ISecurityRightGenerator
    {
        private readonly Dictionary<string, IEnumerable<Model.SecurityRight>> cache;
        private readonly ISecurityRightGenerator decorated;

        public SecurityRightGeneratorCache(ISecurityRightGenerator decorated)
        {
            cache = new Dictionary<string, IEnumerable<Model.SecurityRight>>();
            this.decorated = decorated;
        }

        public IEnumerable<Model.SecurityRight> GetSecurityRights(IEnumerable<string> modules)
        {
            return modules.SelectMany(module => 
                cache.ContainsKey(module) ? 
                    cache[module] : 
                    cache[module] = decorated.GetSecurityRights(new[] { module }));
        }
    }
}
