﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Swh.DataService.SecurityRight.Configuration;

namespace Swh.DataService.SecurityRight.Generator.Modules
{
    internal class ContractSecurityRightGeneratorHelper : SecurityRightGeneratorHelperBase
    {
        public ContractSecurityRightGeneratorHelper(ISecurityRightConfiguration configuration)
            : base(configuration)
        {

        }

        internal override IEnumerable<Model.SecurityRight> SecurityRights
        {
            get
            {
                var assembly = Assembly.LoadFrom(AssemblyPath);

                return GetCommandQueryRights(assembly).Union(GetEntityRights(assembly));
            }
        }

        private static IEnumerable<Model.SecurityRight> GetCommandQueryRights(Assembly assembly)
        {
            var commands = assembly.GetTypes()
                .Where(t => t.Namespace.Contains(".Commands.") && t.Name.EndsWith("Command"));

            var queries = assembly.GetTypes()
                .Where(t => t.Namespace.Contains(".Queries.") && t.Name.EndsWith("Query"));

            return commands.Union(queries)
                .Select(cq =>
                new Model.SecurityRight
                {
                    Scope = cq.Name,
                    Group = Model.SecurityRightGroup.CommandQuery,
                    Type = Model.SecurityRightType.Read,
                });
        }

        private static IEnumerable<Model.SecurityRight> GetEntityRights(Assembly assembly)
        {
            var entityTypes = assembly.GetTypes()
                .Where(t => t.GetInterfaces().Any(i => i.Name == "IDbEntity"));

            return entityTypes.SelectMany(entity => new[]
            {
                new Model.SecurityRight
                {
                    Scope = entity.Name,
                    Group = Model.SecurityRightGroup.Entity,
                    Type = Model.SecurityRightType.Create,
                },
                new Model.SecurityRight
                {
                    Scope = entity.Name,
                    Group = Model.SecurityRightGroup.Entity,
                    Type = Model.SecurityRightType.Delete,
                },
                new Model.SecurityRight
                {
                    Scope = entity.Name,
                    Group = Model.SecurityRightGroup.Entity,
                    Type = Model.SecurityRightType.Export,
                },
                new Model.SecurityRight
                {
                    Scope = entity.Name,
                    Group = Model.SecurityRightGroup.Entity,
                    Type = Model.SecurityRightType.Read,
                },
                new Model.SecurityRight
                {
                    Scope = entity.Name,
                    Group = Model.SecurityRightGroup.Entity,
                    Type = Model.SecurityRightType.Update,
                },
            });
        }
    }
}
