﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class CommandResult
    {
        internal string Message => Match(
            withSuccess: message => message,
            withFailure: exception => exception.Message);

        internal bool IsSuccess => Match(
            withSuccess: _ => true,
            withFailure: _ => false);

        private readonly ICommandResult implementor;

        private CommandResult(ICommandResult implementor)
        {
            this.implementor = implementor;
        }

        internal T Match<T>(Func<string, T> withSuccess, Func<Exception, T> withFailure) =>
            implementor.Match(withSuccess, withFailure);

        private interface ICommandResult
        {
            T Match<T>(Func<string, T> withSuccess, Func<Exception, T> withFailure);
        }

        private class SuccessCommandResult : ICommandResult
        {
            private readonly string message;

            internal SuccessCommandResult(string message)
            {
                this.message = message;
            }

            public T Match<T>(Func<string, T> withSuccess, Func<Exception, T> withFailure)
            {
                return withSuccess(message);
            }

            public static implicit operator CommandResult(SuccessCommandResult success) => 
                new CommandResult(success);
        }

        private class FailureCommandResult : ICommandResult
        {
            private readonly Exception exception;

            internal FailureCommandResult(Exception exception)
            {
                this.exception = exception;
            }

            public T Match<T>(Func<string, T> withSuccess, Func<Exception, T> withFailure)
            {
                return withFailure(exception);

            }

            public static implicit operator CommandResult(FailureCommandResult failure) =>
                new CommandResult(failure);
        }

        internal static CommandResult Success(string message = "") =>
            new SuccessCommandResult(message);

        internal static CommandResult Failure(Exception exception) =>
            new FailureCommandResult(exception);
    }
}
