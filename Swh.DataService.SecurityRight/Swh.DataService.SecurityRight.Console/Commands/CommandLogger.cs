﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Configuration;
using Swh.DataService.SecurityRight.Logging;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class CommandLogger : ICommandHandler
    {
        private readonly ICommandHandler decorated;
        private readonly ISecurityRightConfiguration configuration;
        private readonly ILogger logger;

        public CommandLogger(ICommandHandler decorated, ISecurityRightConfiguration configuration, ILogger logger)
        {
            this.decorated = decorated;
            this.configuration = configuration;
            this.logger = logger;
        }

        public CommandResult Handle(string command)
        {
            try
            {
                logger.Log($"Executing command '{command}' " +
                    $"with AppTenant {configuration.DataService.AppTenantId} " +
                    $"as User {configuration.DataService.UserId}");

                var result = decorated.Handle(command);

                var postHandleLog = result.Match<Action>(
                    withSuccess: message => () => logger.Log($"Finished executing command '{command}' successfully: {message}"),
                    withFailure: exception => () =>
                    {
                        logger.Log($"Failed to execute command '{command}'");

                        logger.LogException(exception);
                    });

                postHandleLog();

                return result;
            }
            catch (Exception e)
            {
                logger.Log($"Execution of command '{command}' resulted in an unhandled exception.");

                logger.LogException(e);

                throw;
            }
        }
    }
}
