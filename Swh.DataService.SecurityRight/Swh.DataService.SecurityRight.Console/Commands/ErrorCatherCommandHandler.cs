﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Logging;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class ErrorCatherCommandHandler : ICommandHandler
    {
        private readonly ICommandHandler decorated;
        private readonly ILogger logger;

        public ErrorCatherCommandHandler(ICommandHandler decorated, ILogger logger)
        {
            this.decorated = decorated;
            this.logger = logger;
        }

        public CommandResult Handle(string command)
        {
            try
            {
                return decorated.Handle(command);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error running command '{command}'");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                return CommandResult.Failure(e);
            }
        }
    }
}
