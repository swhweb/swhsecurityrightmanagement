﻿namespace Swh.DataService.SecurityRight.Configuration
{
    internal interface ISecurtiyRightAssemblyConfiguration
    {
        string AbsolutePath { get; }
        string Name { get; }
        string Switch { get; }
    }
}