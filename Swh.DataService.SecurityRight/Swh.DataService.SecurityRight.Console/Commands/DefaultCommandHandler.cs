﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Commands
{
    internal class DefaultCommandHandler : ICommandHandler
    {
        public CommandResult Handle(string command)
        {
            var exception = new CommandHandlerNotFoundException(command);

            return CommandResult.Failure(exception);
        }
    }
}
