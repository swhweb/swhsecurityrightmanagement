﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Commands;

namespace Swh.DataService.SecurityRight.Application
{
    internal class ResultCode
    {
        #region windows error codes

        internal const int ERROR_SUCCESS = 0;

        internal const int ERROR_INVALID_FUNCTION = 1;

        internal const int ERROR_FILE_NOT_FOUND = 2;

        internal const int ERROR_CAN_NOT_COMPLETE = 1003;

        #endregion

        internal int WindowsErrorCode { get; }

        private ResultCode(int windowsErrorCode)
        {
            WindowsErrorCode = windowsErrorCode;
        }

        internal static ResultCode FromCommandResult(CommandResult result) =>
            result.Match(
                withSuccess: _ => Success,
                withFailure: FromException);

        internal static ResultCode Success { get; } =
            new ResultCode(ERROR_SUCCESS);

        internal static ResultCode FromException(Exception exception)
        {
            //TODO: more specific error codes
            if (exception is CommandHandlerNotFoundException)
            {
                return new ResultCode(ERROR_INVALID_FUNCTION);
            }

            return new ResultCode(ERROR_CAN_NOT_COMPLETE);
        }
    }
}
