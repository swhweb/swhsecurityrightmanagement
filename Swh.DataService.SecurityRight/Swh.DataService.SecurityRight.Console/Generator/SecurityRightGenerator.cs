﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Generator
{
    internal class SecurityRightGenerator : ISecurityRightGenerator
    {
        private readonly IEnumerable<SecurityRightGeneratorHelperBase> helpers;

        public SecurityRightGenerator(IEnumerable<SecurityRightGeneratorHelperBase> helpers)
        {
            this.helpers = helpers;
        }

        public IEnumerable<Model.SecurityRight> GetSecurityRights(IEnumerable<string> modules)
        {
            return helpers.Where(MatchesAny(modules)).SelectMany(helper => helper.SecurityRights);
        }

        private static Func<SecurityRightGeneratorHelperBase, bool> MatchesAny(IEnumerable<string> modules) =>
            (helper) => modules.Contains(helper.GetType().Name.Replace("SecurityRightGeneratorHelper", ""));
    }
}
