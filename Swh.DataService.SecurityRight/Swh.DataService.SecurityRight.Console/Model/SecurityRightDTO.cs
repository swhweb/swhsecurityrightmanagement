﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Model
{
    class SecurityRightDTO
    {
        public Guid Id { get; } = Guid.NewGuid();
        public string Name { get; set; }
        public Guid KeyValueIdSecurityRigthType { get; set; }
        public string EntityName { get; set; }
        public string ViewName { get; set; }
        public string CqName { get; set; }
        public bool BusinessRule { get; set; }
    }
}
