﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Commands;

namespace Swh.DataService.SecurityRight.Application
{
    internal class UserInputExecution : IExecution
    {
        private readonly ICommandHandler commandHandler;

        public UserInputExecution(ICommandHandler commandHandler)
        {
            this.commandHandler = commandHandler;
        }

        public ResultCode Execute()
        {
            while (true)
            {
                Console.Write("> ");

                var command = Console.ReadLine().ToLower();

                if (command == "exit")
                {
                    return ResultCode.Success;
                }
                else
                {
                    var commandResult = commandHandler.Handle(command);

                    Console.WriteLine(commandResult.Message);
                }
            }
        }
    }
}
