﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Configuration;

namespace Swh.DataService.SecurityRight.Generator
{
    internal abstract class SecurityRightGeneratorHelperBase
    {
        private readonly ISecurityRightConfiguration configuration;

        protected string AssemblyPath => configuration.Assemblies.Single(assembly => assembly.Name == GetType().Name.Replace("SecurityRightGeneratorHelper", "")).AbsolutePath;

        internal abstract IEnumerable<Model.SecurityRight> SecurityRights { get; }

        public SecurityRightGeneratorHelperBase(ISecurityRightConfiguration configuration)
        {
            this.configuration = configuration;
        }
    }
}
