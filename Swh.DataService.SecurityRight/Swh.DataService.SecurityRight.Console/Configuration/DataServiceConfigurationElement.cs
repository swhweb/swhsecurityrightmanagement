﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    public class DataServiceConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("appTenantId")]
        public string ApptenantId
        {
            get => (string)this["appTenantId"];
            set => this["appTenantId"] = value;
        }

        [ConfigurationProperty("connectionString")]
        public string ConnectionString
        {
            get => (string)this["connectionString"];
            set => this["connectionString"] = value;
        }

        [ConfigurationProperty("userId")]
        public string UserId
        {
            get => (string)this["userId"];
            set => this["userId"] = value;
        }
    }
}
