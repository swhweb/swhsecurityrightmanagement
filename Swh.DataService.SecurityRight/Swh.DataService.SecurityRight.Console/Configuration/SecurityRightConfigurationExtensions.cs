﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    internal static class SecurityRightConfigurationExtensions
    {
        internal static IEnumerable<string> GetAssemblyNames(
            this ISecurityRightConfiguration configuration, 
            IEnumerable<string> switches)
        {
            return switches.Where(configuration.Assemblies.Select(a => a.Switch).Contains)
                .Select(s => configuration.Assemblies.Single(a => a.Switch == s).Name);
        }
    }
}
