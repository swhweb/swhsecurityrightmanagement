﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Swh.DataService.SecurityRight.Configuration;

namespace Swh.DataService.SecurityRight.Generator.Modules
{
    internal class WebHouseSecurityRightGeneratorHelper : SecurityRightGeneratorHelperBase
    {
        public WebHouseSecurityRightGeneratorHelper(ISecurityRightConfiguration configuration)
            : base(configuration)
        {

        }

        internal override IEnumerable<Model.SecurityRight> SecurityRights
        {
            get
            {
                var assembly = Assembly.LoadFrom(AssemblyPath);

                return CreateMasterControllerViewRights(assembly)
                    .Union(CreateRelationControllerViewRights(assembly))
                    .Union(CreateWorkflowRights(assembly));
            }
        }

        private static IEnumerable<Model.SecurityRight> CreateMasterControllerViewRights(Assembly assembly)
        {
            var masterControllerTypes = assembly.GetTypes()
                .Where(t => !t.IsAbstract)
                .Where(t => IsDecendantOfType(t, "WebHouseMasterControllerBase"));

            return masterControllerTypes.SelectMany(controller =>
            {
                var controllerName = controller.Name.Replace("Controller", "");

                return new[]
                {
                    new Model.SecurityRight
                    {
                         Group = Model.SecurityRightGroup.View,
                         Type = Model.SecurityRightType.Read,
                         Scope = $"{controllerName}Index",
                    },
                    new Model.SecurityRight
                    {
                         Group = Model.SecurityRightGroup.View,
                         Type = Model.SecurityRightType.Read,
                         Scope = $"{controllerName}Edit",
                    },
                    new Model.SecurityRight
                    {
                         Group = Model.SecurityRightGroup.View,
                         Type = Model.SecurityRightType.Read,
                         Scope = $"{controllerName}Read",
                    },
                };
            });
        }

        private static IEnumerable<Model.SecurityRight> CreateRelationControllerViewRights(Assembly assembly)
        {
            var masterControllerTypes = assembly.GetTypes()
                .Where(t => !t.IsAbstract)
                .Where(t => IsDecendantOfType(t, "WebHouseRelationControllerBase"));

            return masterControllerTypes.Select(controller =>
                new Model.SecurityRight
                {
                    Group = Model.SecurityRightGroup.View,
                    Type = Model.SecurityRightType.Read,
                    Scope = $"{controller.Name.Replace("Controller", "")}Index",
                });
        }

        private static IEnumerable<Model.SecurityRight> CreateWorkflowRights(Assembly assembly)
        {
            return new[]
            {
                new Model.SecurityRight
                {
                    Scope= "FtlTransportOrderWorkflow",
                    Group= Model.SecurityRightGroup.View,
                    Type = Model.SecurityRightType.Read,
                },
            };
        }

        private static bool IsDecendantOfType(Type candidate, string ancestorTypeName)
        {
            if (candidate.BaseType == null)
            {
                return false;
            }
            else
            {
                return candidate.BaseType.Name == ancestorTypeName || IsDecendantOfType(candidate.BaseType, ancestorTypeName);
            }
        }
    }
}
