﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.DataService.SecurityRight.Configuration
{
    public class AssemblyConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name")]
        public string Name
        {
            get => (string)this["name"];
            set => this["name"] = value;
        }

        [ConfigurationProperty("absolutePath")]
        public string AbsolutePath
        {
            get => (string)this["absolutePath"];
            set => this["absolutePath"] = value;
        }

        [ConfigurationProperty("switch")]
        public string Switch
        {
            get => (string)this["switch"];
            set => this["switch"] = value;
        }
    }
}
